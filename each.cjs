function each(elements, cb) {
    if (elements.length === 0 || typeof (elements) != 'object') {
        return [];
    }
    for (let index in elements) {
        cb(elements[index], index)
    }
}
module.exports = each;
