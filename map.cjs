function map(elements = [], cb) {
    let newArray = [];
    if (typeof (elements) != 'object' || typeof(cb) != 'function' || cb === undefined) {
        return elements;
    }
    if (elements.length === 0 || !Array.isArray(elements)) {
        return [];
    }
    for (let index = 0; index<elements.length; index++) {
        let element = elements[index];
        newArray.push(cb(element, index, elements));
    }
    return newArray;
}
module.exports = map;