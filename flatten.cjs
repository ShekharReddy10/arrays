function flatten(elements, depth = 1) {
    let resultArray = [];
    if (elements === [] || typeof (elements) != 'object' || elements === undefined) {
        return elements
    }
    let count = depth;
    function recursiveFlatten(elements, depth) {
        for (let index in elements) {
            if (Array.isArray(elements[index])) {
                if (depth > 0) {
                    let decrement = depth-1;
                    recursiveFlatten(elements[index], decrement);
                }
                else {
                    resultArray.push(elements[index]);
                }
            }
            else {
                resultArray.push(elements[index]);
            }
        }
    }
    recursiveFlatten(elements, depth)
    return resultArray;
}
module.exports = flatten;