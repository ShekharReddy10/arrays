var reduce = require('./reduce.cjs');

function cb(startingValue, item, index, elements) {
    return startingValue + item;
}

const items = [1, 2, 3, 4, 5, 5];
console.log(reduce(items, cb, 10));