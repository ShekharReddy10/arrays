function find(elements, cb) {
    if (elements.length === 0 || typeof (elements) != 'object') {
        return []
    }
    for (let index in elements) {
        if (cb(elements[index])) {
            return elements[index];
        }
    }
}
module.exports = find;
