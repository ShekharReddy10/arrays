function reduce(elements = [], cb, startingValue) {
    if (elements.length === 0 || !Array.isArray(elements) || cb === undefined) {
        return [];
    }
    let index = 0;
    if (startingValue === undefined) {
        index = 1;
        startingValue = elements[0];
    }
    for (index; index < elements.length; index++) {
        startingValue = cb(startingValue, elements[index], index, elements);
    }
    return startingValue;
}

module.exports = reduce;