function filter(elements = [], cb) {
    let filterArray = [];
    if (elements.length === 0 || typeof (elements) != 'object' || cb === undefined) {
        return []
    }
    for (let index in elements) {
        if (cb(elements[index], index, elements) === true) {
            filterArray.push(elements[index]);
        }
    }
    return filterArray;
}

module.exports = filter;